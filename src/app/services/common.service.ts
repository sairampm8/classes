import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})


export class CommonService {

  public baserUrl = 'https://jsonplaceholder.typicode.com/posts'

  constructor(
    private http: HttpClient
  ) {

  }

  getSampleData(){
    return this.http.get(this.baserUrl);
  }



  createUser(data){
    return this.http.post(this.baserUrl, data);
  }
}
