import { Component } from '@angular/core';
import { CommonService } from './services/common.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'demo-app';
  userList = [];
  constructor(
    private _commonService: CommonService
  ) {

  }

  ngOnInit(){
    this.getData();
  }

  getData(){
      this._commonService.getSampleData().subscribe(
        (sucess: any)=>{
          //console.log('sucess', sucess);
          this.userList = sucess;
          //console.log('userList', this.userList);
        },
        (error)=>{

        }
      )
  }

  ngOnDestroy(){

  }
}
