import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm = {
    username: '',
    password: ''
  };

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  onLogin(loginData){
    console.log(loginData);
    this.router.navigate(['dashboard', {loginData: loginData.value.username}]);
   
  }
}
