import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  userName = '';
  list =[];


  constructor(
    private currentRouer : ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.currentRouer.paramMap.subscribe(
      (data:any) => {
        if(data.params){
          this.userName = data.params.loginData;
        }
        console.log(data)
      }
    )

    this.getUserDetails();
  }
//usercreation

  navigateUserCreation(){
    this.router.navigate(['usercreation'])
  }

  getUserDetails(){
    const data = JSON.parse(localStorage.getItem('userList'));
    console.log(data);
    this.list = data

  }

  deleteAction(selectedRow){
    console.log(selectedRow);
    const index = this.list.indexOf(selectedRow);
    console.log('index', index);
    this.list.splice(index, 1);
  }

  EditAction(selectedRow){
    console.log(selectedRow);
    this.router.navigate(['usercreation', {edit: JSON.stringify(selectedRow)}])
  }
}
