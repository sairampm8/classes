import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  isEdit = false;
  userForm = {
    username:'',
    password:'',
    email:'',
    id:''
  }
  constructor(
    private router: Router,
    private currentRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.currentRoute.params.subscribe(
      (data)=>{
        if(data && data.edit){
          this.isEdit = true;
          console.log(JSON.parse(data.edit));
          this.userForm = JSON.parse(data.edit);
        }

      }
    )
  }

  createUser(userData: NgForm){
    let usersData= [];
    if(localStorage.getItem('userList')){
      usersData = JSON.parse(localStorage.getItem('userList'));
    }
    if(this.isEdit) {
      const selectedIndex = usersData.findIndex((item)=> item.id === this.userForm.id);
      const tempUpdateObj ={...userData.value};
      tempUpdateObj.id = this.userForm.id;
      usersData.splice(selectedIndex,1, tempUpdateObj);
    }else {
      const tempIdObj = {...userData.value};
      tempIdObj.id =(new Date()).toISOString();
      usersData.push(tempIdObj);
    }
    
    localStorage.setItem('userList',JSON.stringify(usersData));
    this.router.navigate(['dashboard']);
   // console.log(userData.value);
  }

}
