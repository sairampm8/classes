import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Module2routingModule } from './module2routing/module2routing.module';
import { TestComponent } from './components/test/test.component';



@NgModule({
  declarations: [
    TestComponent
  ],
  imports: [
    CommonModule,
    Module2routingModule
  ]
})
export class Module2Module { }
