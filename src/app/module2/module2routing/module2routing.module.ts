import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TestComponent } from '../components/test/test.component';
import { RouterModule, Routes } from '@angular/router';



const routes: Routes = [
  {path:'', pathMatch:'full', redirectTo:'test'},
  {path:'test', component:TestComponent}
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Module2routingModule { }
