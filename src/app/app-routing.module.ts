import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { UserComponent } from './components/user/user.component';

const routes: Routes = [
  {path:'', pathMatch:'full', redirectTo:'dashboard'},
  {path:'login', component:LoginComponent},
  {path:'dashboard', component: DashboardComponent},
  {path:'usercreation', component: UserComponent},
  {
    path: 'customers',
    loadChildren: () => import('./module2/module2.module').then(m => m.Module2Module)
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
